locals {
  global_name = "${var.environment}_${var.product}_${var.application}_${var.use_case}"
  tags = {
    Environment             = var.environment
    Product                 = var.product
    Application             = var.application
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }
}