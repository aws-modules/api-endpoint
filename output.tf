output "aws_api_gateway_resource" {
  value = aws_api_gateway_resource.this.path
}
output "lambda_function_arn" {
  value = aws_lambda_function.this.arn
}
