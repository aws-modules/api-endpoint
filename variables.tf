variable "application" {
  type = string
}
variable "assume_role_policy" {
  type = string
}
variable "bucket_name" {
  type = string
}
variable "environment" {
  type = string
}
variable "handler" {
  type = string
}
variable "memory_size" {
  type = string
}
variable "path_prefix" {
  type = string
}
variable "policy" {
  type = string
}
variable "product" {
  type = string
}
variable "runtime" {
  type = string
}
variable "source_code_dir" {
  type = string
}
variable "timeout" {
  type = string
}
variable "use_case" {
  type = string
}
variable "publish" {
  type    = bool
  default = true
}
variable "environment_variables" {
  type    = map(any)
  default = {}
}
variable "api_gateway_id" {
  type = string
}
variable "api_gateway_root_resource_id" {
  type = string
}
variable "api_gateway_method_http_method" {
  type = string
}
variable "api_gateway_method_authorization" {
  type = string
}
variable "api_gateway_method_authorizer_id" {
  type = string
}
variable "api_gateway_method_api_key_required" {
  type = bool
}
variable "api_gateway_execution_arn" {
  type = string
}
variable "aws_api_gateway_integration_http_method" {
  type = string
}
variable "attach_extra_policy" {
  type = bool
}
variable "allow_origin" {
  type = string
}