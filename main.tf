data "archive_file" "this" {
  type        = "zip"
  source_dir  = var.source_code_dir
  output_path = "${var.path_prefix}/${local.global_name}.zip"
}

resource "aws_s3_bucket_object" "this" {
  bucket = var.bucket_name
  key    = "${local.global_name}.zip"
  source = data.archive_file.this.output_path
  etag   = filemd5("${data.archive_file.this.output_path}")
  tags   = local.tags
}

resource "aws_iam_role_policy" "this" {
  count  = var.attach_extra_policy ? 1 : 0
  name   = local.global_name
  role   = aws_iam_role.this.id
  policy = var.policy
}

resource "aws_iam_role" "this" {
  name               = local.global_name
  assume_role_policy = var.assume_role_policy
  path               = "/"
  description        = local.global_name
  tags               = local.tags
}

resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecutionRole_Policy" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "this" {
  s3_bucket         = var.bucket_name
  s3_key            = aws_s3_bucket_object.this.key
  s3_object_version = aws_s3_bucket_object.this.version_id
  function_name     = local.global_name
  handler           = var.handler
  role              = aws_iam_role.this.arn
  description       = local.global_name
  memory_size       = var.memory_size
  runtime           = var.runtime
  timeout           = var.timeout
  publish           = var.publish
  dynamic "environment" {
    for_each = length(keys(var.environment_variables)) == 0 ? {} : tomap({ "environment_variables" = var.environment_variables })
    content {
      variables = environment.value
    }
  }
  source_code_hash = filebase64sha256(data.archive_file.this.output_path)
  tags             = local.tags
}

resource "aws_api_gateway_resource" "this" {
  rest_api_id = var.api_gateway_id
  parent_id   = var.api_gateway_root_resource_id
  path_part   = var.use_case
}

resource "aws_api_gateway_method" "this" {
  rest_api_id      = var.api_gateway_id
  resource_id      = aws_api_gateway_resource.this.id
  http_method      = var.api_gateway_method_http_method
  authorization    = var.api_gateway_method_authorization
  authorizer_id    = var.api_gateway_method_authorizer_id
  api_key_required = var.api_gateway_method_api_key_required
}

resource "aws_api_gateway_integration" "this" {
  rest_api_id             = var.api_gateway_id
  resource_id             = aws_api_gateway_resource.this.id
  http_method             = var.aws_api_gateway_integration_http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this.invoke_arn
}

resource "aws_lambda_permission" "this" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${var.api_gateway_execution_arn}/*/*/${var.use_case}"
}

module "api-gateway-enable-cors" {
  source          = "squidfunk/api-gateway-enable-cors/aws"
  version         = "0.3.3"
  api_id          = var.api_gateway_id
  api_resource_id = aws_api_gateway_resource.this.id
  allow_methods   = [var.api_gateway_method_http_method]
  allow_origin    = var.allow_origin
}
